<?php

/**
 * Created by PhpStorm.
 * User: test
 * Date: 08.05.2016
 * Time: 12:20
 */
class Collector
{
    const URL = "http://www.cyberphoenix.org/forum/index.php";
    const HASH_REGEX = "/ipb.vars\['secure_hash'\] 		= '(.*)';/";
    const SESSION_ID_REGEX = "/ipb.vars\['session_id'\]			= '(.*)';/";
    const LAST_SHOUT_ID_REGEX = "/id='shout-row-(.*)'><td/";
    const POST_ID_REGEX = "/a id='entry(.*)'></a/";

    public function getSecureHash() {
        $htmlCode = file_get_contents(self::URL);
        preg_match(self::HASH_REGEX, $htmlCode, $matches);
        return $matches[1];
    }

    public function getSessionId() {
        $htmlCode = file_get_contents(self::URL);
        preg_match(self::SESSION_ID_REGEX, $htmlCode, $matches);
        return $matches[1];
    }

    public function getShoutBoxId($htmlCode) {
        preg_match(self::LAST_SHOUT_ID_REGEX, $htmlCode, $matches);
        return $matches[1];
    }

    public function getPostId($htmlCode) {
        preg_match(self::POST_ID_REGEX, $htmlCode, $matches);
        return $matches[1];
    }
}