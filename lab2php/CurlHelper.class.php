<?php

/**
 * Created by PhpStorm.
 * User: test
 * Date: 08.05.2016
 * Time: 12:57
 */
class CurlHelper
{
    const DEBUG_CURL = true;
    const COOKIE_FILE = "cookie.txt";
    const URL = "http://www.cyberphoenix.org/forum/";
    const USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36";


    public function post($url, $referer, $sendString, $isSend = false) {
        $curl = curl_init();
        if( strtolower((substr($url, 0, 5)) == 'https') )
        {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        }
        curl_setopt($curl, CURLOPT_URL, $url);
        if ($isSend) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept:text/javascript, text/html, application/xml, text/xml, */*',
                'Content-Type:application/x-www-form-urlencoded; charset=UTF-8',
                'X-Prototype-Version:1.7.1',
                'X-Requested-With:XMLHttpRequest',
                'Host:www.cyberphoenix.org',
                'Origin:http://www.cyberphoenix.org',
                'Accept-Language:ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
                'Connection:keep-alive',
                'Content-Length:11'));
        }
        curl_setopt($curl, CURLOPT_REFERER, $referer);
        curl_setopt($curl, CURLOPT_VERBOSE, self::DEBUG_CURL);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $sendString);

        curl_setopt($curl, CURLOPT_USERAGENT, self::USER_AGENT);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_COOKIEFILE, self::COOKIE_FILE);
        curl_setopt($curl, CURLOPT_COOKIEJAR, realpath(self::COOKIE_FILE));

        $result=curl_exec($curl);

        curl_close($curl);

        return $result;
    }
}