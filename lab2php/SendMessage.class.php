<?php

/**
 * Created by PhpStorm.
 * User: test
 * Date: 08.05.2016
 * Time: 11:42
 */
include("Collector.class.php");
include("CurlHelper.class.php");

class SendMessage
{
    const SLEEP = 61;
    const COOKIE_FILE = "cookie.txt";
    const URL = "http://www.cyberphoenix.org/forum/";
    const TOPIC_URL = "http://www.cyberphoenix.org/forum/topic/323673-what-is-ur-name-and-your-country/";
    const USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36";
    const DEBUG_CURL = 0;
    const SEND_REFERER = "http://www.cyberphoenix.org/forum/index.php";

    private $secureHash = "";
    private $sessionId = "";
    private $shoutId = "";
    private $collector;
    private $curlHelper;

    public function __construct($login, $pass, $message) {
        $this->collector = new Collector();
        $this->curlHelper = new CurlHelper();

        $this->secureHash = $this->collector->getSecureHash();
        $this->sessionId = $this->collector->getSessionId();

        $htmlAfterLogin = $this->login($login, $pass);
        echo $htmlAfterLogin;
        $this->shoutId = $this->collector->getShoutBoxId($htmlAfterLogin);
    }

    private function login($username, $pass) {
        return $this->curlHelper->post(self::URL, self::URL, "app=core&module=global&section=login&do=process&auth_key="
            . $this->secureHash . "&referer=" . self::URL . "&ips_username=" . $username . "&ips_password=" . $pass);
    }

    public function sendToShoutBox($message) {
        $referer = self::SEND_REFERER . "?s=" . $this->sessionId . "&";
        return $this->curlHelper->post(self::URL, $referer, "s=" . $this->sessionId . "&&app=shoutbox&module=ajax&section=coreAjax&secure_key="
            . $this->secureHash . "&type=submit&lastid=" . $this->shoutId . "&global=1&shout=" . $message, true);
    }

    public function sendToTopic($message) {
        $postId = $this->collector->getPostId(self::TOPIC_URL);
        return $this->curlHelper->post(self::URL, self::TOPIC_URL, "s=" . $this->sessionId . "&app=forums&module=ajax&section=topics&do=reply&t=
        323673&f=6&pid=".$postId."&md5check=".$this->secureHash."&Post=".$message."&isRte=1"."&noSmilies=0");
    }
}