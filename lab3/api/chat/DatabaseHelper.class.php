<?php

/**
 * Created by PhpStorm.
 * User: test
 * Date: 24.05.2016
 * Time: 22:46
 */
class DatabaseHelper
{
    const HOST = "127.0.0.1:3306";
    const USERNAME = "root";
    const PASSWORD = "";
    const DATABASE_NAME = "chat";
    const MESSAGES_TABLE = "messages";
    const MESSAGE_FIELD = "message";
    const ID_FIELD = "id";

    private $connection = null;

    public function __construct() {
        $this->connection = mysqli_connect(self::HOST, self::USERNAME, self::PASSWORD, self::DATABASE_NAME);
        if ($this->connection->connect_error) {
            die("Can't connect to db");
        }
    }

    public function closeConnection() {
        $this->connection->close();
    }

    public function addMessage($message) {
        $query = "INSERT INTO ".self::MESSAGES_TABLE." (".self::MESSAGE_FIELD.") VALUES ('".$message."')";
        return $this->connection->query($query);
    }

    public function getMessagesFromIndex($index) {
        $queryStr = "SELECT * FROM ".self::MESSAGES_TABLE." WHERE ( ".self::ID_FIELD." > ".$index." ) ORDER BY ".self::ID_FIELD;
        $sqlResult = $this->connection->query($queryStr);
        $newMessages = array();
        while ($row = $sqlResult->fetch_assoc()) {
            array_push($newMessages, $row[self::MESSAGE_FIELD]);
        }
        return $newMessages;
    }
}
