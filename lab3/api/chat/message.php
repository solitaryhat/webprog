<?php
/**
 * Created by PhpStorm.
 * User: test
 * Date: 15.05.2016
 * Time: 17:42
 */
    include("DatabaseHelper.class.php");

    $user = $_POST['user'];
    $action = $_POST['action'];
    $successfulResponse = array("status" => "success", "message" => "");
    $failureResponse = array("status" => "failure", "message" => "");

    header('Content-Type: application/json');
    $dbHelper = new DatabaseHelper();
    $response = array();
    switch ($action) {
        case "send":
            $message = $_POST['message'];
            if ($message == null) {
                $failureResponse["message"] = "Expected message";
                $response = $failureResponse;
            } else {
                $transformedMessage = $user." : ".$message;
                $dbHelper->addMessage($transformedMessage);
                $response = $successfulResponse;
            }
            break;
        case "getNewMessages":
            $index = $_POST['index'];
            if ($index == null) {
                $failureResponse["message"] = "Expected index";
                $response = $failureResponse;
            } else {
                $successfulResponse["messages"] = $dbHelper->getMessagesFromIndex($index);
                $response = $successfulResponse;
            }
            break;
        default:
            $failureResponse["message"] = "Unexpected action";
            $response = $failureResponse;
            break;
    }
    $dbHelper->closeConnection();
    echo json_encode($response);
