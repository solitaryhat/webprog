<?php
/**
 * Created by PhpStorm.
 * User: test
 * Date: 15.05.2016
 * Time: 17:41
 */
    
    const USERS = array("user1" => "1qwerty", "user2" => "1qwerty");
    $user = $_POST["user"];
    $password = $_POST["password"];
    $successfulResponse = array("status" => "success", "user" => "");
    $failureResponse = array("status" => "failure", "message" => "");
    
    header('Content-Type: application/json');
    if (array_key_exists($user, USERS)) {
        if (USERS[$user] == $password) {
            $successfulResponse["user"] = $user;
            echo json_encode($successfulResponse);
        } else {
            $failureResponse["message"] = "Incorrect password";
            echo json_encode($failureResponse);
        }
    } else {
        $failureResponse["message"] = "Username not found";
        echo json_encode($failureResponse);
    }
