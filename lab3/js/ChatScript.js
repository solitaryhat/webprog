/**
 * Created by test on 09.05.2016.
 */
var user = "";
var count = 0;

function login() {
    var url = "api/authorization/login.php";
    event.preventDefault();
    var username = $('#login_username').val();
    var passwordd = $('#login_pass').val();
    var dataForPost = {user:username, password:passwordd};
    $.ajax({
        type: 'POST',
        url: url,
        data: dataForPost,
        dataType: 'json',
        success: function (response) {
            if (response.status != "success") {
                alert(response.message);
            } else {
                showChat();
                user = username;
                setInterval(update, 1000)
            }
        },
        error: function () {
            alert("Something went wrong in login");
            console.log("Something went wrong on server in login");
        }
    });
    return true;
}

function update() {
    var url = 'api/chat/message.php';
    var chat = $('#chat_box');
    var countAsString = count.toString();
    var dataForPost = {user:user, action:'getNewMessages', index:countAsString};
    $.ajax({
        type :'POST',
        url: url,
        data: dataForPost,
        dataType: 'json',
        success: function (response) {
            if (response.status != "success") {
                alert(response.message);
            } else {
                console.log("Successful getMessages");
                for (var i = 0; i < response.messages.length; ++i) {
                    chat.val(chat.val() +  response.messages[i] + '\n');
                    count++;
                    chat.scrollTop(chat.scrollHeight);
                }
            }
        },
        error: function () {
            console.log("Something went wrong on server in update");
        }
    });
}

function sendMessage() {
    event.preventDefault();
    var url = 'api/chat/message.php';
    var chat = $('#chat_box');
    var messageField = $('#message_box');
    var message = messageField.val();
    chat.val(chat.val() + user + ' : ' +  message + '\n');
    chat.scrollTop(-chat.scrollHeight);
    messageField.val('');
    var dataForPost = {user:user, action:'send', message:message};
    $.ajax({
        type :'POST',
        url: url,
        data: dataForPost,
        dataType: 'json',
        success: function (response) {
            if (response.status != "success") {
                alert(response.message);
            } else {
                console.log("Send success");
                count++;
            }
        },
        error: function () {
            console.log("Something went wrong on server in sendMessage");
        }
    });
    return true;
}