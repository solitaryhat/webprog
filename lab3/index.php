<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Chat</title>
        <link rel="stylesheet" href="css/stylesheet.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="js/ChatScript.js"></script>
        <script src="js/PageController.js"></script>
    </head>
    <body onload="hideChat()">
        <div class="login_form" id="login_form">
            <form id="login">
                <h3>Sign in!</h3>
                Login : <br> <input type="text" id="login_username"> <br>
                Password : <br> <input type="text" id="login_pass"> <br> <br>
                <input  type="submit" value="Sign In!!!" onclick="login()">
            </form>
            <p id="login_results"></p>
        </div>

        <div class="chat_form" id="chat_form">
            <form id="chat">
                <textarea disabled id="chat_box" class="chat_box" rows="10" cols="30"></textarea>
                <br>
                Message : <input id="message_box" class="message" type="text">
                <br>
                <input id="submit_button" type="submit" class="submit" value="Send" onclick="sendMessage()">
            </form>
        </div>
    </body>
</html>